(function() {
  'use strict';

  angular
    .module('learninghub')
    .directive('course', course);

  /** @ngInject */
  function course() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/course/course.html',
      scope: {
        course: '=',
        upVote: '&',
        downVote: '&'
      }
    };
    return directive;
  }

})();
