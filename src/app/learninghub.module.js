(function() {
  'use strict';

  angular
    .module('learninghub', ['ngCookies', 'ngRoute', 'toastr', 'ngStorage']);

})();
