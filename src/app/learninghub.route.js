(function() {
  'use strict';

  angular
    .module('learninghub')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/learninghub/learninghub.html',
        controller: 'LearningHubController',
        controllerAs: 'learningHubCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
