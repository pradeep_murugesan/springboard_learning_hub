(function() {
  'use strict';

  angular
    .module('learninghub')
    .controller('LearningHubController', LearningHubController);

  /** @ngInject */
  function LearningHubController($log, learninghubSvc, $localStorage, toastr) {
    var vm = this;

    function activate() {
      getCourses();
    }
    activate();
    function getCourses() {
      learninghubSvc.getCourses().then(function (data) {
        $log.info(data);
          vm.courses = data.paths;
          toastr.success("Successfully fetched " + vm.courses.length + " paths");
          applyLocalStorage();
        }).catch(function (data) {
          $log.error(data);
        });
    }

    vm.upVote = function(course) {
      if(course.upvote) {
        course.upvote++;
      } else {
        course.upvote = 1;
      }
      vote("upVote", course.id);
    };

    vm.downVote = function(course) {
      if(course.downVote) {
        course.downVote++;
      } else {
        course.downVote = 1;
      }
      vote("downVote", course.id);
    };

    function vote(type, id) {
      if(!$localStorage.courses[id]) {
        $localStorage.courses[id] = {}
      } else {
        if(!$localStorage.courses[id][type]) {
          $localStorage.courses[id][type] = 1;
        } else {
          $localStorage.courses[id][type]++;
        }
      }
    }


    function applyLocalStorage() {
      if($localStorage.courses && Object.keys($localStorage.courses).length != 0) {
        toastr.warning("applied the local data");
        vm.courses.forEach(function(course){
          if($localStorage.courses.hasOwnProperty(course.id)) {
            var storedCourse = $localStorage.courses[course.id];
            if(storedCourse.hasOwnProperty("upVote")) {
              course.upvote = storedCourse["upVote"];
            }
            if(storedCourse.hasOwnProperty("downVote")) {
              course.downVote = storedCourse["downVote"];
            }
          }
        })
      } else {
        $localStorage.courses = {}
      }
    }

  }
})();
