(function() {
  'use strict';

  angular
    .module('learninghub')
    .factory('learninghubSvc', learninghubSvc);

  /** @ngInject */
  function learninghubSvc($log, $http) {
    var api = 'https://hackerearth.0x10.info/api/learning-paths?type=json&query=list_paths';
    var service = {
      getCourses: getCourses
    };

    return service;

    function getCourses() {
      return $http.get(api)
        .then(getCoursesComplete)
        .catch(getCoursesFailed);

      function getCoursesComplete(response) {
        return response.data;
      }

      function getCoursesFailed(error) {
        $log.error('XHR Failed for get websites.\n' + angular.toJson(error.data, true));
      }
    }
  }
})();
